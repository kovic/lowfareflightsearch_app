﻿using LOFF_app.Model;
using LOFF_app.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOFF_app.ModeView
{
    /// <summary>
    /// Handles data to be displayed in the UI
    /// </summary>
    public static class ViewModel
    {
        /// <summary>
        /// Handles communication with API and returns the result
        /// </summary>
        /// <param name="query">Current query</param>
        /// <returns></returns>
        public static async Task<List<FlightInfo>> GetQueryResults(Query query)
        {
            // Set API path
            string path = ApiHelper.ApiKey + query.GetQueryString();

            // Wait for response from API
            using (HttpResponseMessage response = await ApiHelper.Client.GetAsync(path))
            {
                if (response.IsSuccessStatusCode)
                {
                    // If response was successful, parse the info and setup JSON object
                    var json = await response.Content.ReadAsStringAsync();
                    var jsonObject = JObject.Parse(json);
                    
                    // Get flight count
                    int flightCount = jsonObject["results"].Count();

                    // Instantiate flight list
                    var flightList = new List<FlightInfo>();

                    // Loop through each flight, instantiate FlightInfo object and set its properties 
                    for (int i = 0; i < flightCount; i++)
                    {
                        // Get property values from JSON
                        int outboundFlightNum = jsonObject["results"][i]["itineraries"][0]["outbound"]["flights"].Count();
                        string depAirport = (string)jsonObject["results"][i]["itineraries"][0]["outbound"]["flights"][0]["origin"]["airport"];
                        string arrAirport = (string)jsonObject["results"][i]["itineraries"][0]["outbound"]["flights"][outboundFlightNum - 1]["destination"]["airport"];
                        string depDate = (string)jsonObject["results"][i]["itineraries"][0]["outbound"]["flights"][0]["departs_at"];

                        var flightInfo = new FlightInfo()
                        {
                            DepartureAirport = Cache.GetAirportString(depAirport),
                            ArrivalAirport = Cache.GetAirportString(arrAirport),
                            DepartureDate = GetDateInfoString(depDate),
                            DepartureFlightChanges = outboundFlightNum.ToString(),
                            NumberOfPassengers = query.PassengerCount.ToString(),
                            TotalPrice = (string)jsonObject["results"][i]["fare"]["total_price"],
                            Currency = (string)jsonObject["currency"],                            
                        };

                        // Add info about the return flight if one exists, if not, set placeholder values
                        if (query.HasReturnFlight)
                        {
                            int inboundFlightNum = 0;
                            inboundFlightNum = jsonObject["results"][i]["itineraries"][0]["inbound"]["flights"].Count();                            
                            string retDate = (string)jsonObject["results"][i]["itineraries"][0]["inbound"]["flights"][inboundFlightNum - 1]["departs_at"];
                            flightInfo.ReturnDate = GetDateInfoString(retDate); 
                            flightInfo.ReturnFlightChanges = inboundFlightNum.ToString();
                        }
                        else
                        {
                            flightInfo.ReturnDate = "N/A";
                            flightInfo.ReturnFlightChanges = "N/A";
                        }

                        // Add flight info to the list
                        flightList.Add(flightInfo);
                    }
                    return flightList;
                }
                // If API couldn't return flight info, display error message to the user and prompt him for correct input 
                else
                {
                    // Parse json
                    var json = await response.Content.ReadAsStringAsync();
                    var jsonObject = JObject.Parse(json);
                    // Get error message from API
                    string message = (string)jsonObject["message"];
                    // Show message for user
                    var messageWindow = new MessageWindow(message);
                    messageWindow.Show();
                    return null;                    
                }
            }
            
        }

        /// <summary>
        /// Converts received date string into more readable format
        /// </summary>
        /// <param name="oldDateString">Old date string</param>
        /// <returns></returns>
        public static string GetDateInfoString(string oldDateString)
        {
            // Try converting old string into a new format, if it fails, return old format
            try
            {
                string newDataString = oldDateString.Replace("T", " , ");
                return newDataString;
            }
            catch
            {
                return oldDateString;
            }            
        }
    }
}
