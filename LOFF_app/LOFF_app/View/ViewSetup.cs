﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOFF_app.View
{
    /// <summary>
    /// Helper class for UI setup
    /// </summary>
    public static class ViewSetup
    {
        /// <summary>
        /// Load currencies into a string array
        /// </summary>
        /// <returns></returns>
        public static string[] LoadCurrencies()
        {
            var currencyArray = new string[] { "USD", "EUR", "HRK"};

            return currencyArray;
        }
    }
}
