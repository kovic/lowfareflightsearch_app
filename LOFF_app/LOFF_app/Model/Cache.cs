﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOFF_app.Model
{
    /// <summary>
    /// Handles data caching
    /// </summary>
    public static class Cache
    {
        // Holds cached key-value pairs for Queries and corresponding FlightLists
        private static IDictionary<Query, List<FlightInfo>> _flightListCache = new Dictionary<Query, List<FlightInfo>>(); // TODO: use hash table
        // Holds cached key-value pairs for IATA codes and airporst
        private static IDictionary<string, string> _airportDictionary = new Dictionary<string, string>();

        /// <summary>
        /// Checks if query has been cached
        /// </summary>
        /// <param name="query">Current query</param>
        /// <returns></returns>
        public static bool IsQueryCached(Query query)
        {
            // Loop through each cached query and check if it corresponds to the current one
            foreach (var qry in _flightListCache)
            {
                if (qry.Key.Equals(query))
                {                    
                    return true;
                }
            }            
            return false;
        }

        /// <summary>
        /// Caches the current Query and corresponding FlightList
        /// </summary>
        /// <param name="query">Current query</param>
        /// <param name="flightList">Resulting flight list</param>
        public static void CacheQuery(Query query, List<FlightInfo> flightList)
        {
            _flightListCache.Add(query, flightList);
        }

        /// <summary>
        /// Returns the flight list corresponding to the given query
        /// </summary>
        /// <param name="query">Current query</param>
        /// <returns></returns>
        public static List<FlightInfo> GetCachedFlightList (Query query)
        {
            return _flightListCache[query];
        }

        /// <summary>
        /// Initializes airport list
        /// </summary>
        public static void InitializeAirportDictionary()
        {
            // Parse json file into JSON array
            JArray jsonObject = JArray.Parse(File.ReadAllText(Path.Combine(Directory.GetCurrentDirectory(), "../../Data/airports.json")));
            int size = jsonObject.Count();
            // Loop through all airportst from json array and add appropriate fields to the list
            for (int i = 0; i < size; i++)
            {
                _airportDictionary.Add((string)jsonObject[i]["iata"], (string)jsonObject[i]["airport"]);
            }
        }

        /// <summary>
        /// Get airport name based on IATA code
        /// </summary>
        /// <param name="iataCode">IATA code</param>
        /// <returns></returns>
        public static string GetAirportString(string iataCode)
        {
            // Try getting the airport name based on IATA code
            try
            {
                string airport = _airportDictionary[iataCode];
                // If list doesn't contain airport name for a given IATA code, just return IATA code
                if (airport == string.Empty)
                    return iataCode;
                else
                    return airport;
            }
            // If list doesnt have current IATA code, just return it
            catch
            {
                return iataCode;
            }            
        }
    }
}
