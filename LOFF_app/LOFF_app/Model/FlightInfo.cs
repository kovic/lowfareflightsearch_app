﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOFF_app.Model
{
    /// <summary>
    /// Holds data of a given flight
    /// </summary>
    public class FlightInfo
    {        
        public string DepartureAirport { get; set; }
        public string ArrivalAirport { get; set; }
        public string DepartureDate { get; set; }
        public string ReturnDate { get; set; }
        public string DepartureFlightChanges { get; set; }
        public string ReturnFlightChanges { get; set; }
        public string NumberOfPassengers { get; set; }
        public string TotalPrice { get; set; }
        public string Currency { get; set; }
    }    
}
