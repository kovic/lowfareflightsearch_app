﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LOFF_app.Util;
using Newtonsoft.Json.Linq;

namespace LOFF_app.Model
{
    /// <summary>
    /// Query based on user input upon which the flight search will be done
    /// </summary>
    public class Query
    {
        // Constants
        private const string _DEP_ARPT_STRING_PREFIX = "origin="; 
        private const string _ARR_ARPT_STRING_PREFIX = "destination=";
        private const string _DEP_DATE_STRING_PREFIX = "departure_date="; 
        private const string _RET_DATE_STRING_PREFIX = "return_date=";
        private const string _PASSENGER_STRING_PREFIX = "adults=";
        private const string _CURRENCY_STRING_PREFIX = "currency=";
        private const int _HASH = 1;

        // Private fields
        private string _departAirport;
        private string _arrAirport;        
        private string _currency;

        // Properties
        public string DepartAirport
        {
            get { return _departAirport; }
            set { _departAirport = value != null ? _DEP_ARPT_STRING_PREFIX + value : string.Empty; }
        }
        public string ArriveAirport
        {
            get { return _arrAirport; }
            set { _arrAirport = value != null ? _ARR_ARPT_STRING_PREFIX + value : string.Empty; }
        }
        public string Currency
        {
            get { return _currency; }
            set { _currency = value != null ? _CURRENCY_STRING_PREFIX + value : string.Empty; }
        }
        public int PassengerCount { get; set; }
        public DateTime? DepartureDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public bool HasReturnFlight { get; private set; }              

        /// <summary>
        /// Query constructor
        /// </summary>
        /// <param name="hasReturnFlight">Does current query include return flight</param>
        public Query(bool hasReturnFlight)
        {            
            HasReturnFlight = hasReturnFlight;
        }

        /// <summary>
        /// Returns query string for Amadeus API
        /// </summary>
        /// <returns></returns>
        public string GetQueryString()
        {
            // Set string for date and passenger count 
            string depDateString = GetDateString(DepartureDate, _DEP_DATE_STRING_PREFIX); 
            string passengerCountString = GetPassengerString();         
                         
            string queryString = string.Format("{0}&{1}&{2}&{3}&{4}", DepartAirport, ArriveAirport, depDateString, passengerCountString, Currency);

            // Add return flight info if query has return flight
            if (HasReturnFlight)
            {
                string retDateString = GetDateString(ReturnDate, _RET_DATE_STRING_PREFIX);
                queryString = string.Format("{0}&{1}", queryString, retDateString);
            }

            return queryString;            
        }

        /// <summary>
        /// Compares two queries based on their properties
        /// </summary>
        /// <param name="obj">Current query</param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var query = obj as Query;
            if (this.DepartAirport != query.DepartAirport)
                return false;
            if (this.ArriveAirport != query.ArriveAirport)
                return false;
            if (this.DepartureDate != query.DepartureDate)
                return false;
            if (this.PassengerCount != query.PassengerCount)
                return false;
            if (this.Currency != query.Currency)
                return false;
            if (this.HasReturnFlight != query.HasReturnFlight)
                return false;
            else
            {
                if (this.ReturnDate != query.ReturnDate)
                    return false;
            }            
            return true;            
        }

        /// <summary>
        /// Sets hash code for query. 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            // TODO: Implement this correctly, using uniqe hashes
            return _HASH;
        }

        /// <summary>
        /// Returns query string for passanger count
        /// </summary>
        /// <returns></returns>
        private string GetPassengerString()
        {
            return _PASSENGER_STRING_PREFIX + PassengerCount.ToString();
        }

        /// <summary>
        /// Returns query string for date value
        /// </summary>
        /// <param name="date">Date of flight</param>
        /// <param name="prefix">String prefix need for query string</param>
        /// <returns></returns>
        private string GetDateString(DateTime? date, string prefix)
        {
            return date.HasValue ? prefix + date.Value.ToString("yyyy-MM-dd") : String.Empty;
        }
    }
}
