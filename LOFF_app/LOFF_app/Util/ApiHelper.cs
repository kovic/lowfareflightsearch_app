﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LOFF_app.Util
{
    /// <summary>
    /// Initializes objects need for communication with web API
    /// </summary>
    public class ApiHelper
    {
        // User key need for API access
        private const string _KEY = "?apikey=6ZueepJvH0caN9WFFAGBDGtseZ6Rd0ja&";

        // User key access property
        public static string ApiKey { get { return _KEY; } }

        public static HttpClient Client { get; private set; }

        /// <summary>
        /// Initialize HTTP client and set URI and headers
        /// </summary>
        public static void Initialize()
        {
            Client = new HttpClient()
            {
                BaseAddress = new Uri("https://api.sandbox.amadeus.com/v1.2/flights/low-fare-search")
            };
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Add("application", "json");
        }
    }
}
