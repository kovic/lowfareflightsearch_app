﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LOFF_app.Model;
using LOFF_app.ModeView;
using LOFF_app.Util;
using LOFF_app.View;

namespace LOFF_app
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            // Center window
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            // Inirialize HTTP client
            ApiHelper.Initialize();
            // Instantiate airport list            
            Cache.InitializeAirportDictionary();

            // Hide data grid info
            processingTextBlock.Visibility = Visibility.Hidden;
            flightDataGrid.Visibility = Visibility.Hidden;
            // Load currencies
            CurrencyTextBox.ItemsSource = ViewSetup.LoadCurrencies();
        }

        private async void QueryButton_Click(object sender, RoutedEventArgs e)
        {
            var message = string.Empty;
            // Validate input and display appropriate message or continue with execution if input is valid
            if (ValidateInput(out message))
            {
                // Handle UI elements
                processingTextBlock.Visibility = Visibility.Visible;
                queryGrid.Visibility = Visibility.Hidden;                
                
                // Instantiate query
                Query query = new Query(returnFlightCheckBox.IsChecked.Value)
                {
                    DepartAirport = departureAirportTextBox.Text,
                    ArriveAirport = arrivalAirportTextBox.Text,
                    DepartureDate = departureDateTextBox.SelectedDate,
                    ReturnDate = returnDateTextBox.SelectedDate,
                    PassengerCount = int.Parse(numberOfPassengersTextBox.Text),
                    Currency = CurrencyTextBox.SelectedValue.ToString()                    
                };
                
                // Check if query is already cached
                if (Cache.IsQueryCached(query)) 
                {
                    // If query is cached, display cached data
                    processingTextBlock.Visibility = Visibility.Hidden;
                    returnToSearchButton.Visibility = Visibility.Visible;
                    flightDataGrid.Visibility = Visibility.Visible;
                    flightDataGrid.ItemsSource = Cache.GetCachedFlightList(query); 
                }
                // Query is not cached
                else
                {
                    // Instantiate flight info list
                    List<FlightInfo> flightList = await ViewModel.GetQueryResults(query);                    
                    if (flightList != null)
                    {
                        // If API returned valid info, cache it and display it on the screen
                        Cache.CacheQuery(query, flightList);
                        processingTextBlock.Visibility = Visibility.Hidden;
                        returnToSearchButton.Visibility = Visibility.Visible;
                        flightDataGrid.Visibility = Visibility.Visible;
                        flightDataGrid.ItemsSource = flightList;
                    }
                    else
                    {
                        // Activate query grid if API didn't return valid info
                        processingTextBlock.Visibility = Visibility.Hidden;
                        queryGrid.Visibility = Visibility.Visible;
                    }
                }
            }
            else
            {
                // Show appropriate error message to the user if input was invalid
                var messageWindow = new MessageWindow(message);
                messageWindow.Show();
            }
        }

        /// <summary>
        /// Validates input
        /// </summary>
        /// <param name="message">Message to be returned if input is invalid</param>
        /// <returns></returns>
        private bool ValidateInput(out string message)
        {
            message = string.Empty;
            var result = 0;
            // Try parsing passenger count
            bool isNumber = int.TryParse(numberOfPassengersTextBox.Text, out result);

            // Departure airport check
            if (departureAirportTextBox.Text == string.Empty)
            {
                message = "Please enter the departure airport.";
                return false;
            }
            // Destination airport check
            if (arrivalAirportTextBox.Text == string.Empty)
            {
                message = "Please enter the destination airport.";
                return false;
            }
            // Departure date check
            if (departureDateTextBox.SelectedDate == null)
            {
                message = "Please enter the departure date.";
                return false;
            }
            // Passenger number chekck. 
            if (isNumber)
            {
                // Check if number was zero or less
                if (result <= 0)
                {
                    message = "Passenger number has to be at least 1.";
                    return false;
                }
            }
            // Number wasn't entered
            else
            {
                message = "Please enter the number of passegers.";
                return false;
            }
            // Currency check
            if (CurrencyTextBox.SelectedValue == null)
            {
                message = "Please enter the currency.";
                return false;
            }
            // Return flight check
            if ((bool)returnFlightCheckBox.IsChecked)
            {
                if (returnDateTextBox.SelectedDate == null)
                {
                    message = "Please enter the return date.";
                    return false;
                }
            }                        
            return true;
        }

        /// <summary>
        /// Handle return flight checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HasReturnFlight_Toggle(object sender, RoutedEventArgs e)
        {
            returnDateTextBox.IsEnabled = (bool)returnFlightCheckBox.IsChecked;
        }

        /// <summary>
        /// Handle return to search button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReturnToSearchButton_Click(object sender, RoutedEventArgs e)
        {
            // Clear grid data
            flightDataGrid.ItemsSource = null;
            // Show query grid and hide data grid and return button
            queryGrid.Visibility = Visibility.Visible;
            flightDataGrid.Visibility = Visibility.Hidden;
            returnToSearchButton.Visibility = Visibility.Hidden;
        }
    }
}
